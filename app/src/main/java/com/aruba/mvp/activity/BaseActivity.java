package com.aruba.mvp.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.widget.ImageView;

import com.aruba.mvp.R;
import com.aruba.mvp.presenter.HttpPresenter;
import com.aruba.mvp.util.ImageCache;
import com.aruba.mvp.util.StatusBarUtil;
import com.aruba.mvp.util.dialog.InfoDialog;
import com.aruba.mvp.util.dialog.Popupwindow;
import com.aruba.mvp.view.IBaseView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;


public class BaseActivity extends AppCompatActivity implements IBaseView {
    protected Popupwindow popupwindow;

    private InfoDialog infoDialog;

    private List<HttpPresenter> httpPresenters = new ArrayList<>(1);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTranslucent(this);
//        //强制竖屏
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
    }


    /**
     * 自定义跳转动�?
     *
     * @param intent
     * @param in     :进入动画
     * @param out
     */
    public void startActivity(Intent intent, int in, int out) {

        super.startActivity(intent);
        overridePendingTransition(in, out);
    }

    /**
     * 使用默认动画
     *
     * @param intent
     * @param isDefault
     */
    public void startActivity(Intent intent, boolean isDefault) {

        super.startActivity(intent);
        overridePendingTransition(R.anim.activity_translate_in,
                R.anim.activity_translate_out);
    }

    /**
     * 使用默认动画
     *
     * @param intent
     * @param isDefault
     */
    public void startActivityForResult(Intent intent, int requestCode, boolean isDefault) {

        super.startActivityForResult(intent, requestCode);
        overridePendingTransition(R.anim.activity_translate_in,
                R.anim.activity_translate_out);
    }

    /**
     * 带有动画效果的�??出activity
     */
    public void finishWithAnim() {
        // TODO Auto-generated method stub
        super.finish();
        overridePendingTransition(R.anim.activity_translate_in,
                R.anim.activity_translate_out);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
            overridePendingTransition(R.anim.activity_translate_in,
                    R.anim.activity_translate_out);
            return true;
        } else {
            return false;
        }
    }

    public void showDialog() {
        if (popupwindow == null) {
            popupwindow = new Popupwindow(this);
        }
        popupwindow.show();
    }

    public void showDialog(String content) {
        if (popupwindow == null) {
            popupwindow = new Popupwindow(this, content);
        } else {
            popupwindow.setContent(content);
        }
        popupwindow.show();
    }

    public void dismissDialog() {
        if (popupwindow != null && popupwindow.isShowing()) {
            popupwindow.disMiss();
        }
    }


    public void showInfoDialog(String content) {
        if (infoDialog == null) {
            infoDialog = new InfoDialog.Builder(this)
                    .onlySure(true)
                    .hideTitle(true)
                    .content(content)
                    .build();
        } else {
            infoDialog.setContent(content);
        }

        if (infoDialog.isShowing()) {
            infoDialog.disMiss();
        }

        infoDialog.show();
    }

    public void dismissInfoDialog() {
        if (infoDialog != null && infoDialog.isShowing()) {
            infoDialog.disMiss();
        }
    }

    public void loadBitmap(int resId, ImageView imageView) {
        ImageCache.getInstance().loadBitmap(resId, imageView);
    }

    @Override
    public void showLoading() {
        showDialog();
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void showInfo(String msg) {
        showInfoDialog(msg);
    }

    @Override
    public void showErr(String uri, String msg) {
        showInfoDialog(msg);
    }

    @Override
    public Context getContext() {
        return this.getApplicationContext();
    }

    public void addPresenter(HttpPresenter httpPresenter) {
        if (httpPresenter != null) {
            httpPresenter.attachView(this);
            httpPresenters.add(httpPresenter);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (httpPresenters.size() != 0) {
            for (HttpPresenter httpPresenter : httpPresenters) {
                if (httpPresenter != null) {
                    httpPresenter.detachView();
                }
            }
        }
    }
}
