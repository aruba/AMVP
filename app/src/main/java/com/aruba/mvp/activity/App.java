package com.aruba.mvp.activity;

import android.app.Application;


/**
 * 作者　　: aruba
 * 创建时间:2017/6/5　16:46
 * <p>
 * 功能介绍：
 */
public class App extends Application {

    private static App instance;

    public static App getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        instance = this;
        super.onCreate();

    }
}
