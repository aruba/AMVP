package com.aruba.mvp.model;

import android.os.Handler;
import android.os.Looper;

import com.aruba.mvp.util.http.GsonHelper;
import com.aruba.mvp.util.http.NovateHelper;
import com.tamic.novate.Throwable;
import com.tamic.novate.callback.RxStringCallback;
import com.tamic.novate.download.DownLoadCallBack;
import com.tamic.novate.exception.NovateException;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by aruba on 2018/2/6.
 */

public abstract class BaseModel {
    //数据请求参数
    protected HashMap<String, Object> mParams = new HashMap<>();

    public BaseModel() {
        this.mParams = mParams;
    }

    /**
     * 设置数据请求参数
     *
     * @param mParams 参数数组
     */
    public BaseModel params(HashMap<String, Object> mParams) {
        this.mParams = mParams;
        return this;
    }

    public void execute(String url, Object rxResultCallback) {
        
    }

    public void execute(String url, String fileName, Object rxResultCallback) {

    }

    // 执行Get网络请求，此类看需求由自己选择写与不写
    public void requestGetAPI(String url, RxStringCallback rxResultCallback) {
        NovateHelper.getInstance().getNovate().rxGet(url, url, mParams, rxResultCallback);
    }

    // 执行Post网络请求，此类看需求由自己选择写与不写
    public void requestPostAPI(String url, RxStringCallback rxResultCallback) {
        NovateHelper.getInstance().getNovate().rxPost(url, url, mParams, rxResultCallback);
    }

    public void requestJsonAPI(String url, RxStringCallback rxResultCallback) {
        NovateHelper.getInstance().getNovate().rxJson(url, url, GsonHelper.getInstance().getGson().toJson(mParams), rxResultCallback);
    }

    protected Object tag;

    public void requestFileAPI(String url, final RxStringCallback rxResultCallback) {
        tag = url;
        MultipartBody.Builder builder = new MultipartBody.Builder();
        Set<Map.Entry<String, Object>> entrySet = mParams.entrySet();

        for (Map.Entry<String, Object> entry : entrySet) {
            if (entry.getValue() instanceof File) {
                File file = (File) entry.getValue();
                builder.addFormDataPart(entry.getKey(), file.getName(),
                        RequestBody.create(MediaType.parse("multipart/form-data"), file));//添加文件
            } else {
                if (entry.getValue() != null)
                    builder.addFormDataPart(entry.getKey(), String.valueOf(entry.getValue()));
            }
        }

        //构建body
        RequestBody requestBody = builder
                .build();

        final okhttp3.Request request = new okhttp3.Request.Builder()
                .url(url)
                .addHeader("Content-Type", "application/json")
//                .addHeader("Token", UnitSystemConfig.token)
                .post(requestBody)
                .build();

        OkHttpClient client = new OkHttpClient();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(final Call call, final IOException e) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        rxResultCallback.onError(tag, NovateException.handleException(e));
                    }
                });
            }

            @Override
            public void onResponse(final Call call, final Response response) throws IOException {
                if (call.isCanceled()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            rxResultCallback.onCancel(tag, new Throwable(null, -200, "已取消"));
                        }
                    });
                }

                if (rxResultCallback.isReponseOk(tag, response.body())) {

                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                rxResultCallback.onNext(tag, rxResultCallback.onHandleResponse(response.body()));
                            } catch (Exception e) {
                                e.printStackTrace();
                                rxResultCallback.onError(tag, NovateException.handleException(e));
                            }
                        }
                    });

                }
            }
        });

//        NovateHelper.getInstance().getNovate().rxBody(url, url, requestBody, rxResultCallback);
    }


    public void downloadFileAPI(String url, String fileName, DownLoadCallBack downLoadCallBack) {
        String name = (String) mParams.get(fileName);
        NovateHelper.getInstance().getNovate().download(url, name, downLoadCallBack);
    }

}
