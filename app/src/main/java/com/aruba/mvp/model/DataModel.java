package com.aruba.mvp.model;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by aruba on 2018/2/6.
 */

public class DataModel {

    public static BaseModel invoke(Class aClass) {
        // 声明一个空的BaseModel
        BaseModel model = null;
        try {
            Class clazz = Class.forName(aClass.getName());
            Constructor c = clazz.getConstructor();
            model = (BaseModel) c.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return model;
    }
}
