package com.aruba.mvp.model;

import com.tamic.novate.callback.RxStringCallback;

/**
 * Created by aruba on 2018/7/11.
 */

public class CustomModel extends BaseModel {
    
    @Override
    public void execute(String url, Object rxResultCallback) {
        requestJsonAPI(url, (RxStringCallback) rxResultCallback);
    }

}
