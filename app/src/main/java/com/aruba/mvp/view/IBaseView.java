package com.aruba.mvp.view;

import android.content.Context;

/**
 * Created by aruba on 2018/2/6.
 */

public interface IBaseView {
    /**
     * 显示正在加载view
     */
    void showLoading();

    /**
     * 关闭正在加载view
     */
    void hideLoading();

    /**
     * 显示提示
     *
     * @param msg
     */
    void showInfo(String msg);

    /**
     * 显示请求错误提示
     */
    void showErr(String uri, String msg);

    /**
     * 获取上下文
     *
     * @return 上下文
     */
    Context getContext();
}
