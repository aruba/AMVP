package com.aruba.mvp.view;

import com.aruba.mvp.util.http.HttpResponse;

/**
 * Created by aruba on 2020/5/11.
 */

public interface TestView extends IBaseView {

    void testDataCallBack(HttpResponse data);
}
