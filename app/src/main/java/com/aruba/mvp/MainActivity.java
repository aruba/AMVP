package com.aruba.mvp;

import android.os.Bundle;

import com.aruba.mvp.activity.BaseActivity;
import com.aruba.mvp.presenter.TestPresenter;
import com.aruba.mvp.util.http.HttpResponse;
import com.aruba.mvp.view.TestView;

import java.util.HashMap;

public class MainActivity extends BaseActivity implements TestView {

    TestPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new TestPresenter();
        addPresenter(presenter);

        presenter.getData("haha", new HashMap<String, Object>());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void testDataCallBack(HttpResponse data) {

    }
}
