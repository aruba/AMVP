package com.aruba.mvp.presenter;

import com.aruba.mvp.model.CustomModel;
import com.aruba.mvp.model.DataModel;
import com.aruba.mvp.util.http.BaseDataCallback;
import com.aruba.mvp.util.http.HttpResponse;
import com.aruba.mvp.view.TestView;

import java.util.HashMap;

/**
 * Created by aruba on 2018/2/6.
 */

public class TestPresenter extends HttpPresenter {

    public void getData(String url, HashMap<String, Object> params) {
        realGetData(DataModel.invoke(CustomModel.class).params(params), url, new BaseDataCallback<HttpResponse>() {
            @Override
            public void dataCallback(HttpResponse data) {
                ((TestView) getView()).testDataCallBack(data);
            }
        });
    }
}
