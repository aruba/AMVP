package com.aruba.mvp.util.http;

/**
 * Created by aruba on 2018/7/11.
 */

public abstract class BaseDataCallback<T> {
    public abstract void dataCallback(T data);

    public void emptyCallBack(String info) {
    }

    public void failDataCallBack(T data) {
    }

    public boolean needFailData;
}
