package com.aruba.mvp.util.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;

import com.aruba.mvp.R;


/**
 * 共通dialog
 * creator:aruba
 * time:2016.2.3
 */
public class CustomDialog extends Dialog {

    public CustomDialog(Context context) {
        super(context);
    }

    public CustomDialog(Context context, int theme) {
        super(context, theme);
    }

    public static class Builder {
        private Context context;
        private OnClickListener positiveButtonClickListener;
        private OnClickListener negativeButtonClickListener;
        private String content;
        private int contentid;
        private int layoutRes;
        private int PositiveButtonId;
        private int NegativeButtonId;

        public Builder(Context context, int layoutRes) {
            this.context = context;
            this.layoutRes = layoutRes;
        }


        /**
         * Set the positive button resource and it's listener
         *
         * @param PositiveButtonId
         * @return
         */
        public Builder setPositiveButton(int PositiveButtonId, OnClickListener listener) {
            this.PositiveButtonId = PositiveButtonId;
            this.positiveButtonClickListener = listener;
            return this;
        }

        public Builder setPositiveButton(String positiveButtonText, OnClickListener listener) {
            this.positiveButtonClickListener = listener;
            return this;
        }

        public Builder setNegativeButton(int NegativeButtonId, OnClickListener listener) {
            this.NegativeButtonId = NegativeButtonId;
            this.negativeButtonClickListener = listener;
            return this;
        }

        public void setContent(String content, int id) {
            this.content = content;
            this.contentid = id;
        }

        public CustomDialog create() {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // instantiate the dialog with the custom Theme
            final CustomDialog dialog = new CustomDialog(context, R.style.Dialog);
            View layout = inflater.inflate(layoutRes, null);
            dialog.addContentView(layout, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));


            // set the confirm button
            if (positiveButtonClickListener != null) {
                (layout.findViewById(PositiveButtonId)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        positiveButtonClickListener.onClick(dialog, DialogInterface.BUTTON_POSITIVE);
                    }
                });
            }
            if (negativeButtonClickListener != null) {
                (layout.findViewById(NegativeButtonId)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        negativeButtonClickListener.onClick(dialog, DialogInterface.BUTTON_NEGATIVE);
                    }
                });
            }

            if (!TextUtils.isEmpty(content) && contentid != 0) {
                ((TextView) layout.findViewById(contentid)).setText(content);
            }

            dialog.setContentView(layout);
            return dialog;
        }

    }


}
