package com.aruba.mvp.util.http;

import android.content.Context;

import com.tamic.novate.Novate;

/**
 * Created by aruba on 2018/7/16.
 */

public class NovateHelper {
    private static String base_url = "";
    private static NovateHelper instance;
    private Novate novate;

    public static NovateHelper getInstance() {
        if (instance == null) {
            instance = new NovateHelper();
        }
        return instance;
    }

    public void init(Context context, String baseUrl) {
        this.base_url = baseUrl;

        novate = new Novate.Builder(context.getApplicationContext())
                .baseUrl(base_url)
                .skipSSLSocketFactory(true)
                .addLog(true)
                .addCache(false)
                .build();
    }

    public Novate getNovate() {
        return novate;
    }

    public ToMainClear toMainClear;

    public void setToMainClear(ToMainClear toMainClear) {
        this.toMainClear = toMainClear;
    }

    public interface ToMainClear {
        void toMainClear();
    }

}
