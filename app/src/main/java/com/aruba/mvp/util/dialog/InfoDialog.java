package com.aruba.mvp.util.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.aruba.mvp.R;


/**
 * 提示框dialog
 * creator:aruba
 * time:2017.6.28
 */
public class InfoDialog {
    private CustomDialog loadingdialog;
    private Context mContext;
    private TextView sure;
    private TextView cancel;
    private TextView tv_title;
    private TextView tv_content;
    private ActionCallBack actionCallBack;

    public InfoDialog(Context context) {
        init(context, "");
    }

    public InfoDialog(Context context, String content) {
        init(context, content);
    }

    private void init(Context context, String content) {
        mContext = context;

        CustomDialog.Builder builder = new CustomDialog.Builder(mContext, R.layout.dialogwindow);
        loadingdialog = builder.create();

        tv_title = (TextView) loadingdialog.findViewById(R.id.tv_title);
        sure = (TextView) loadingdialog.findViewById(R.id.sure);
        cancel = (TextView) loadingdialog.findViewById(R.id.cancel);

        View.OnClickListener onClickListener = getDefaultClickListener();
        sure.setOnClickListener(onClickListener);
        cancel.setOnClickListener(onClickListener);
        loadingdialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                cancel.callOnClick();
            }
        });

        tv_content = (TextView)
                loadingdialog.findViewById(R.id.tv_content);
        if (!TextUtils.isEmpty(content))
            tv_content.setText(content);

        //设置dialog居中显示
        Window dialogWindow = loadingdialog.getWindow();
        dialogWindow.setGravity(Gravity.CENTER);
    }

    public void setContent(String content) {
        tv_content.setText(content);
    }

    public boolean isShowing() {
        return loadingdialog.isShowing();
    }

    public View.OnClickListener getDefaultClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disMiss();
            }
        };
    }

    public void setSureText(String text) {
        sure.setText(text);
    }

    public void setCancelText(String text) {
        cancel.setText(text);
    }

    public void setActionCallBack(ActionCallBack actionCallBack) {
        this.actionCallBack = actionCallBack;
        sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InfoDialog.this.actionCallBack.sureCallBack();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InfoDialog.this.actionCallBack.cancelCallBack();
            }
        });
    }

    public void setOnlySure() {
        View line_vertical = loadingdialog.findViewById(R.id.line_vertical);
        line_vertical.setVisibility(View.GONE);
        cancel.setVisibility(View.GONE);
    }

    /**
     * 隐藏
     */
    public void disMiss() {
        if (loadingdialog != null && loadingdialog.isShowing())
            loadingdialog.dismiss();
    }

    public void show() {
        if (loadingdialog != null && !loadingdialog.isShowing()) {
            loadingdialog.show();
        }
    }

    public void setSureOnclickListener(View.OnClickListener onclickListener) {
        sure.setOnClickListener(onclickListener);
    }

    public void setCancelOnclickListener(View.OnClickListener onclickListener) {
        cancel.setOnClickListener(onclickListener);
    }

    public void hideTitle() {
        tv_title.setVisibility(View.GONE);
    }

    public interface ActionCallBack {
        void sureCallBack();

        void cancelCallBack();
    }

    public static class Builder {
        private Context mContext;
        private String content = "";
        private boolean isHideTitle;
        private boolean isOnlySure;
        private String sureText;
        private String cancelText;
        private ActionCallBack actionCallBack;
        private View.OnClickListener sureClickListener;
        private View.OnClickListener cancelClickListener;

        public Builder(Context mContext) {
            this.mContext = mContext;
        }

        public Builder content(String content) {
            this.content = content;
            return this;
        }

        public Builder hideTitle(boolean isHideTitle) {
            this.isHideTitle = isHideTitle;
            return this;
        }

        public Builder onlySure(boolean isOnlySure) {
            this.isOnlySure = isOnlySure;
            return this;
        }

        public Builder sureText(String sureText) {
            this.sureText = sureText;
            return this;
        }

        public Builder cancelText(String cancelText) {
            this.cancelText = cancelText;
            return this;
        }

        public Builder actionCallBack(ActionCallBack actionCallBack) {
            this.actionCallBack = actionCallBack;
            return this;
        }

        public Builder sureClickListener(View.OnClickListener sureClickListener) {
            this.sureClickListener = sureClickListener;
            return this;
        }

        public Builder cancelClickListener(View.OnClickListener cancelClickListener) {
            this.cancelClickListener = cancelClickListener;
            return this;
        }

        public InfoDialog build() {
            InfoDialog infoDialog = new InfoDialog(mContext, content);
            if (isHideTitle) {
                infoDialog.hideTitle();
            }
            if (sureText != null) {
                infoDialog.setSureText(sureText);
            }
            if (cancelText != null) {
                infoDialog.setCancelText(cancelText);
            }
            if (actionCallBack != null) {
                infoDialog.setActionCallBack(actionCallBack);
            }
            if (sureClickListener != null) {
                infoDialog.setSureOnclickListener(sureClickListener);
            }
            if (cancelClickListener != null) {
                infoDialog.setCancelOnclickListener(cancelClickListener);
            }
            if (isOnlySure) {
                infoDialog.setOnlySure();
            }
            return infoDialog;
        }
    }

}
