package com.aruba.mvp.util.dialog;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.aruba.mvp.R;


/**
 * 加载中dialog
 * creator:aruba
 * time:2016.2.3
 */
public class Popupwindow {
    private CustomDialog loadingdialog;

    private TextView tv_writeupload;
    private ProgressBar progressBar2;
    private String type;
    public static final String FINISH = "FINISH";
    private Context mContext;

    /**
     * use Popupwindow(Context context,  String content,
     * final int ms, String type)
     *
     * @param context
     * @param view
     * @param content
     * @param ms
     * @param type
     */
    @Deprecated
    public Popupwindow(Context context, View view, String content,
                       int ms, String type) {
        init(context, content);
        this.type = type;
        progressBar2.setVisibility(View.GONE);


        show();
        runThread(ms);
    }

    /**
     * use Popupwindow(Context context,  String content)
     *
     * @param context
     * @param view
     * @param content
     */
    @Deprecated
    public Popupwindow(Context context, View view, String content) {
        init(context, content);
        initPopup(content);
    }

    public Popupwindow(Context context, String content,
                       final int ms, String type) {
        init(context, content);
        this.type = type;
        progressBar2.setVisibility(View.GONE);


        show();
        runThread(ms);
    }

    public Popupwindow(Context context) {
        init(context, "");
    }

    public Popupwindow(Context context, String content) {
        init(context, content);
    }

    public Popupwindow(Context context, String content, int ms) {
        init(context, content);
        progressBar2.setVisibility(View.GONE);

        //设置dialog居中显示
        Window dialogWindow = loadingdialog.getWindow();
        dialogWindow.setGravity(Gravity.CENTER);


        show();
        runThread(ms);
    }

    public Popupwindow(Context context, String content, int ms, boolean showProgress) {
        init(context, content);
        if (!showProgress) {
            progressBar2.setVisibility(View.GONE);
        }

        //设置dialog居中显示
        Window dialogWindow = loadingdialog.getWindow();
        dialogWindow.setGravity(Gravity.CENTER);


        show();
        runThread(ms);
    }

    private void init(Context context, String content) {
        mContext = context;

        CustomDialog.Builder builder = new CustomDialog.Builder(mContext, R.layout.waitupload);
        loadingdialog = builder.create();

        tv_writeupload = (TextView)
                loadingdialog.findViewById(R.id.tv_writeupload);
        if (!TextUtils.isEmpty(content))
            tv_writeupload.setText(content);
        progressBar2 = (ProgressBar) loadingdialog
                .findViewById(R.id.progressBar2);

        loadingdialog.setCancelable(false);
        loadingdialog.setCanceledOnTouchOutside(false);
        //设置dialog居中显示
        Window dialogWindow = loadingdialog.getWindow();
        dialogWindow.setGravity(Gravity.CENTER);
    }

    private void runThread(final int ms) {
        new Thread() {
            public void run() {
                try {
                    this.sleep(ms);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                handler.sendEmptyMessage(0);
            }

            ;
        }.start();
    }

    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            disMiss();
            if (type != null && type.equals(FINISH)) {
                try {
                    Activity activity = (Activity) mContext;
                    activity.finish();
                } catch (Exception e) {
                    Log.e("popupwindowErr", "该activity已被消亡");
                }
            }
        }

        ;
    };

    /**
     * use show
     *
     * @param info
     */
    @Deprecated
    public void initPopup(String info) {
        show();
    }

    public void show() {
        if (loadingdialog != null && !loadingdialog.isShowing()) {
            try {
                loadingdialog.show();
            } catch (Exception e) {

            }
        }
    }

    public void setContent(String content) {
        if (tv_writeupload != null)
            tv_writeupload.setText(content);
    }

    /**
     * 隐藏
     */
    public void disMiss() {
        if (loadingdialog != null && loadingdialog.isShowing())
            loadingdialog.dismiss();
    }

    public boolean isShowing() {
        if (loadingdialog != null)
            return loadingdialog.isShowing();

        return false;
    }

}
