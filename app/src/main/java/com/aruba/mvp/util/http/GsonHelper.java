/**
 * Copyright (C) 2006-2014 TongCheng All rights reserved
 */
package com.aruba.mvp.util.http;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonHelper {
	private Gson mGson;
	private static GsonHelper instance = null;

	private GsonHelper() {
		mGson = new GsonBuilder().disableHtmlEscaping().create();
	}

	public static synchronized GsonHelper getInstance() {
		if (instance == null) {
			instance = new GsonHelper();
		}
		return instance;
	}

	public Gson getGson() {
		return mGson;
	}
}
